import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LabreqHService {

  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async list(datas:object) {
//     datas:any={
//   "limit":100,
//   "patient_id":"",
//   "cid":"",
//   "hn":"",
//   "name":"",
//   "lname":"",
//   "HosCode":"",

// }
    const _url = `${this.apiUrl}/Labreq_h/list`;
    return this.httpClient.post(_url,datas, this.httpOptions).toPromise();
  }

  async save(datas: object) {
    const _url = `${this.apiUrl}/Labreq_h`;
    return this.httpClient.post(_url, datas, this.httpOptions).toPromise();
  }

  // async update(code: any, data: object) {
  //   const _url = `${this.apiUrl}/lookup_amphur/${code}`;
  //   return this.httpClient.put(_url, data, this.httpOptions).toPromise();
  // }

  // async remove(code: any) {
  //   const _url = `${this.apiUrl}/lookup_amphur/${code}`;
  //   return this.httpClient.delete(_url, this.httpOptions).toPromise();
  // }


}