import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async list(datas:object) {
//     datas:any={
//   "limit":100,
//   "patient_id":"",
//   "cid":"",
//   "hn":"",
//   "name":"",
//   "lname":"",
//   "HosCode":"",

// }
    const _url = `${this.apiUrl}/users/list`;
    return this.httpClient.post(_url,datas, this.httpOptions).toPromise();
  }

  async save(datas: object) {
    const _url = `${this.apiUrl}/users`;
    return this.httpClient.post(_url, datas, this.httpOptions).toPromise();
  }

  async update(user_id: any, data: object) {
    const _url = `${this.apiUrl}/users/${user_id}`;
    return this.httpClient.put(_url, data, this.httpOptions).toPromise();
  }

  async remove(user_id: any) {
    const _url = `${this.apiUrl}/users/${user_id}`;
    return this.httpClient.delete(_url, this.httpOptions).toPromise();
  }

  async login(username:any,password:any){
    let data:any = {
      username:username,
      password:password
    }
    const _url = `${this.apiUrl}/login`;
    return this.httpClient.post(_url, data, this.httpOptions).toPromise();
  }


}