import { TestBed } from '@angular/core/testing';

import { LookupTypeitemService } from './lookup-typeitem.service';

describe('LookupTypeitemService', () => {
  let service: LookupTypeitemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LookupTypeitemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
