import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LabresultService {

  token: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
      })
    };
  }

  async list(datas:object) {
//     {
//       "req_no":"650218133",
//       "txtSearch":""
// }
    const _url = `${this.apiUrl}/labresult/list`;
    return this.httpClient.post(_url,datas, this.httpOptions).toPromise();
  }


}