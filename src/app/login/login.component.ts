import { Component, OnInit } from '@angular/core';
import { UsersService} from '../shared/users.service';
import { AlertService} from '../shared/alert.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { default as swal, SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username:any;
  password:any;


  constructor(
    private usersService : UsersService,
    private alertService : AlertService,
    private router:Router,
    private jwtHelper:JwtHelperService
  ) { }

  ngOnInit(): void {
  }

  async login(){
    if(this.username && this.password){
      try {
        let rs:any = await this.usersService.login(this.username,this.password);
        console.log(rs);
        // let token:any = JSON.stringify(rs);
        if(rs.token){
          const decoded: any = this.jwtHelper.decodeToken(rs.token);
          console.log(decoded);
          
          sessionStorage.setItem('UserCode', decoded.UserCode);
          sessionStorage.setItem('Fullname', decoded.FirstName+" "+decoded.LastName);
          sessionStorage.setItem('HosCode', decoded.HosCode);
          sessionStorage.setItem('token',rs.token);
          this.router.navigate(["/home"]);
        }
      } catch (error:any) {
        console.log(error);
        this.alertService.error(error.error.message);
      }
    }
  }

}
