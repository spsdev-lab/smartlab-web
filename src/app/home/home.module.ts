import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { ClarityModule } from '@clr/angular';
import { HomeRoutingModule } from './home-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { PatientComponent } from './patient/patient.component';
import { HomeComponent } from './home.component';
import { CheckresultComponent } from './checkresult/checkresult.component';
import { ResultmanualComponent } from './resultmanual/resultmanual.component';
import { Importordercovid19Component } from './importordercovid19/importordercovid19.component';
import { ExportorderComponent } from './exportorder/exportorder.component';
import { PrintsummaryorderComponent } from './printsummaryorder/printsummaryorder.component';
import { SummaryallhospitalComponent } from './summaryallhospital/summaryallhospital.component';
import { SummarybyhospitalComponent } from './summarybyhospital/summarybyhospital.component';
import { SummaryalltestComponent } from './summaryalltest/summaryalltest.component';
import { SummarybytestComponent } from './summarybytest/summarybytest.component';
import { OrderentryComponent } from './orderentry/orderentry.component';
import { HospitalComponent } from './hospital/hospital.component';
import { CommentComponent } from './comment/comment.component';
import { GroupsComponent } from './groups/groups.component';
import { SpecimensComponent } from './specimens/specimens.component';
import { LabsComponent } from './labs/labs.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { AppointComponent } from './appoint/appoint.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { BookingComponent } from './booking/booking.component';
import { ClrIconModule } from '@clr/angular';
import { SampleresultComponent } from './sampleresult/sampleresult.component';



@NgModule({
  declarations: [
    LayoutComponent,
    PatientComponent,
    HomeComponent,
    CheckresultComponent,
    ResultmanualComponent,
    Importordercovid19Component,
    ExportorderComponent,
    PrintsummaryorderComponent,
    SummaryallhospitalComponent,
    SummarybyhospitalComponent,
    SummaryalltestComponent,
    SummarybytestComponent,
    OrderentryComponent,
    HospitalComponent,
    CommentComponent,
    GroupsComponent,
    SpecimensComponent,
    LabsComponent,
    ProfilesComponent,
    UseraccountComponent,
    AppointComponent,
    HolidaysComponent,
    BookingComponent,
    SampleresultComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    ClarityModule,
    NgbModule,
    FormsModule,
    ClrIconModule

  ]
})
export class HomeModule { }
