import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryalltestComponent } from './summaryalltest.component';

describe('SummaryalltestComponent', () => {
  let component: SummaryalltestComponent;
  let fixture: ComponentFixture<SummaryalltestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryalltestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryalltestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
