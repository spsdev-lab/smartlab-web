import { Component, OnInit } from '@angular/core';
import { AlertService} from '../../shared/alert.service';
import {LookupLabSpecimentService} from '../../shared/lookup-lab-speciment.service';

@Component({
  selector: 'app-specimens',
  templateUrl: './specimens.component.html',
  styleUrls: ['./specimens.component.scss']
})
export class SpecimensComponent implements OnInit {
  txtimage:any;
  txtcode:any;
  txtspecimenname:any;
  txtlabgroup:any;
  txtenabled:any;
  specimen_id:any;
  itemSpecimen:any=[];
  openModal:boolean=false;
  constructor(
    private alertService:AlertService,
    private lookupLabSpecimentService:LookupLabSpecimentService
  ) { }

  ngOnInit(): void {
    this.ListSpecimen();
  }

async ListSpecimen(){
    this.itemSpecimen = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupLabSpecimentService.list(info);
      console.log(rs);
      this.itemSpecimen = rs.rows;
      console.log('itemSpecimen :',this.itemSpecimen);


      
    } catch (error) {
      console.log('itemSpecimen :',error);
      
    }
  }

  async savespecimen(){
    let confirm:any=await this.alertService.confirm('ท่านต้องการบันทึกหรือไม่');
    if(confirm){
    let commentdata:any={
            "Image": this.txtcode,
            "Code": this.txtcode,
            "Specimen_Name": this.txtspecimenname,
            "Enabled": this.txtenabled,
            "Lab_Group": this.txtlabgroup,
            
    }
    try {
      console.log(commentdata);
      
      let rs:any=await this.lookupLabSpecimentService.save(commentdata);
      console.log(rs);
    } catch (error) {
      console.log(error);
      
    }}
  }


  async formUpdate(specimen:any){
    this.openModal=true;
    this.txtimage=specimen.Image;
    this.txtcode = specimen.Code;
    this.txtspecimenname=specimen.Specimen_Name;
    this.txtenabled=specimen.Enabled;
    this.txtlabgroup=specimen.Lab_Group;
    if(specimen.Enabled=="YES"){
      this.txtenabled=true;
    }else{
      this.txtenabled=false;
    }
    this.specimen_id=specimen.Code;
  }



  async updatespecimen(){
    
    let confirm:any=await this.alertService.confirm('ท่านต้องการแก้ไขหรือไม่');
    if(confirm){
      let enabled:any;
      if(this.txtenabled){
        enabled="YES";
      }else{
        enabled="NO";
      }
    let specimendata:any={
            "Image": this.txtimage,
            "Code": this.txtcode,
            "Specimen_Name": this.txtspecimenname,
            "Lab_Group": this.txtlabgroup,
            "Enabled": enabled,

            
    }
    try {
      console.log(specimendata);
      
      let rs:any=await this.lookupLabSpecimentService.update(this.specimen_id,specimendata);
      console.log(rs);
      this.openModal=false;
this.ListSpecimen();
    } catch (error) {
      console.log(error);
      
    }
  }
  }




  async frmSpecimen(){
    this.openModal=true;
    console.log('frmSpecimen',this.openModal);
    
  }

}
