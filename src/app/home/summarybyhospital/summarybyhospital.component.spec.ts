import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarybyhospitalComponent } from './summarybyhospital.component';

describe('SummarybyhospitalComponent', () => {
  let component: SummarybyhospitalComponent;
  let fixture: ComponentFixture<SummarybyhospitalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummarybyhospitalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarybyhospitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
