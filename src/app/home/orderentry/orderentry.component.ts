import { Component, OnInit } from '@angular/core';
import {LookupHoscodeService} from '../../shared/lookup-hoscode.service';
import {LookupLabSitService} from '../../shared/lookup-lab-sit.service';
import {LookupCommentService} from '../../shared/lookup-comment.service';
import {LookupLabGroupService} from '../../shared/lookup-lab-group.service';
import {LookupLabService} from '../../shared/lookup-lab.service';
import {PatientService} from '../../shared/patient.service';
import {LookupLabSpecimentService} from '../../shared/lookup-lab-speciment.service';
import {LabreqHService} from '../../shared/labreq-h.service';
import {LabreqDService} from '../../shared/labreq-d.service';
import {AlertService} from '../../shared/alert.service';
import * as moment from 'moment-timezone';


@Component({
  selector: 'app-orderentry',
  templateUrl: './orderentry.component.html',
  styleUrls: ['./orderentry.component.scss']
})
export class OrderentryComponent implements OnInit {
  itemHoscode:any=[];
  itemLabSit:any=[];
  itemComment:any=[];
  itemLabGroup:any=[];
  itemLabSpecimentCHE:any=[];
  itemLabCHE:any=[];
  itemLabIMM:any=[];
  itemLabMIC:any=[];
  itemLabBIO:any=[];
  itemLabMIS:any=[];
  itemLabHEM:any=[];
  itemLabOUT:any=[];
  itemLab:any=[];
  checklab:any = 'checklab';
  req_lab:any=[];

  hcode:String=' ';
  sit_id:any;
  patient_id:any;
  cid:any;
  comment_id:any;
  fullname:any;
  txtage:any;
  today:any=new Date();
  itemPatient:any=[];
  che:any;
  txthn:any;
  UserCode:any;
  Fullname:any;
  HosCode:any;


  constructor(
    private patientService:PatientService,
    private lookupHoscodeService:LookupHoscodeService,
    private lookupLabSitService:LookupLabSitService,
    private lookupCommentService:LookupCommentService,
    private lookupLabGroupService:LookupLabGroupService,
    private lookupLabService:LookupLabService,
    private lookupLabSpecimentService:LookupLabSpecimentService,
    private labreqHService:LabreqHService,
    private labreqDService:LabreqDService,
    private alertService:AlertService
    
    ) { 

      this.Fullname = sessionStorage.getItem('Fullname');
      this.UserCode = sessionStorage.getItem('UserCode');
      this.HosCode = sessionStorage.getItem('HosCode');
      this.hcode=this.HosCode;
    }

  ngOnInit(): void {
    this.ListlookupHoscode();
    this.ListlookupLabSit();
    this.ListlookupComment();
    this.ListlookupLabSpecimentCHE();
    this.ListlookupLabCHE();
    this.ListlookupLabIMM();
    this.ListlookupLabMIC();
    this.ListlookupLabBIO();
    this.ListlookupLabMIS();
    this.ListlookupLabHEM();
    this.ListlookupLabOUT();
    this.ListlookupLab();

  }

  async savelab(){
    let inforeq_h:any={
      "HOSCode": this.hcode,
      "username": this.UserCode,
      "patient_id":this.patient_id,
      "cid": this.cid,
      "lab_type": "",
      "tot_req": "3",
      "req_date": moment.tz(this.today, "Asia/Bangkok").format('YYYYMMDD'),
      "req_time": moment.tz(this.today, "Asia/Bangkok").format('HHmm'),
      "lconfirm": "N",
      "labUnit": null,
      "cfm_user": "",
      "cfm_date": "",
      "cfm_time": "",
      "reqSysID": "JAYNLAB",
      "HIS_LabNo": "000000000000000",
      "CoLab_receive_department_code": "",
      "CoLab_receive_department_name": "",
      "CoLab_speciment": "",
      "CoLab_objective": "",
      "CoLab_loinc": "",
      "CoLab_collection_place": "",
      "CoLab_recieve_date": null,
      "CoLab_vn": "",
    }  
    console.log(inforeq_h);
    
    try {
      let rs:any=await this.labreqHService.save(inforeq_h);
      
      console.log(rs);
      console.log(this.req_lab);
      let x:any=0;
      for(const l of this.req_lab){
        
        let inforeq_d:any={
          "req_no":rs.rows[0].req_no,
          "num_req": x+1,
          "cid": this.cid,
          "patient_id":this.patient_id,
          "lab_code": l.Code,
          "reverse_flag": null,
          "lab_name": l.Lab_Name,
          "res_ready": "N",
          "date": moment.tz(this.today, "Asia/Bangkok").format('YYYYMMDD'),
          "time": moment.tz(this.today, "Asia/Bangkok").format('HHmm'),
          "lab_type": null,
          "labSeqNo": null,
          "cmdSend": null,
          "hpackDet": null,
          "hpackReq": null,
          "endReq": null,
          "usrName": this.UserCode,
          "app_flag": null,
          "chgCode": null,
          "authFlag": null,
          "authDateTime": null,
          "authUser": null
      }

      let rs_req_d:any = await this.labreqDService.save(inforeq_d);
      console.log(rs_req_d);

      }

      if(rs.ok){
        this.alertService.success('บันทึกสำเร็จ');
        this.patient_id=null;
        this.cid=null;
        this.req_lab = [];
        window.location.reload();
      }
      
      
      
    } catch (error) {
      console.log(
        error
      );
      
    }
  }


  async onChanged(lab:any){
    // console.log(lab);
    // console.log(lab.checkCHE);

      if(lab.checklab){
          this.req_lab.push(lab);   
          //  console.log(lab);

      }else{
        let itemReq_lab = this.req_lab;
        this.req_lab = [];
        // console.log(this.req_lab);
        
        for( const l of itemReq_lab){
          // console.log("l",l);
          // console.log("itemReq_lab",itemReq_lab);

          
          if(l.Code == lab.Code){
            
          }else{
            this.req_lab.push(l)
          }

        }
      }
    console.log(this.req_lab);
  }


  async ListPatient(){
    this.itemPatient = [];
    let info:any={
      limit:"1000",
      HIS_HN:this.txthn
    };

    try {
      let rs:any = await this.patientService.list(info);
      console.log(rs);
      this.itemPatient = rs.rows;
      console.log('itemPatient :',this.itemPatient);

      this.fullname=`${rs.rows[0].name} ${rs.rows[0].lname}`;

      this.patient_id=rs.rows[0].patient_id;
      this.cid=rs.rows[0].cid;

      let xx:any = (moment().diff(rs.rows[0].birth, 'months') - (moment().diff(rs.rows[0].birth, 'years')*12));
      let dd:any;
      if( xx == 1){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+31));
      }else if(xx == 2){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+59));
      }else if(xx == 3){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+90));
      }else if(xx == 4){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+120));
      }else if(xx == 5){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+151));
      }else if(xx == 6){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+181));
      }else if(xx == 7){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+212));
      }else if(xx == 8){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+243));
      }else if(xx == 9){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+273));
      }else if(xx == 10){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+304));
      }else if(xx == 11){
        dd = (moment().diff(rs.rows[0].birth, 'days')-((moment().diff(rs.rows[0].birth, 'years')*365)+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+334));
      }

      console.log(dd);
      
      this.txtage=moment().diff(rs.rows[0].birth, 'years')+"ปี "+(moment().diff(rs.rows[0].birth, 'months')-(moment().diff(rs.rows[0].birth, 'years')*12))+"เดือน "+ (dd)+"วัน";

    
      
    } catch (error) {
      console.log('itemPatient :',error);
      
    }
  }

  async ListlookupHoscode(){
    this.itemHoscode = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupHoscodeService.list(info);
      //console.log(rs);
 
      this.itemHoscode = rs.rows;
      // this.itemHoscode.push({hos_id:' ',hos_name:'กรุณาเลือก',Enabled:''});
      console.log('itemHoscode :',this.itemHoscode);


      
    } catch (error) {
      console.log('itemHoscode :',error);
      
    }
  }

  async ListlookupLabSit(){
    this.itemLabSit = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupLabSitService.list(info);
      //console.log(rs);
      this.itemLabSit = rs.rows;
      console.log('itemLabSit :',this.itemLabSit);


      
    } catch (error) {
      console.log('itemLabSit :',error);
      
    }
  }

  async ListlookupComment(){
    this.itemComment = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupCommentService.list(info);
      //console.log(rs);
      this.itemComment = rs.rows;
      console.log('itemComment :',this.itemComment);


      
    } catch (error) {
      console.log('itemComment :',error);
      
    }
  }

  async ListlookupLabSpecimentCHE(){
    this.itemLabSpecimentCHE= [];
    let info:any={
      limit:"1000",
      LabGroup:'2'

    };

    try {
      let rs:any = await this.lookupLabSpecimentService.list(info);
      //console.log(rs);
      this.itemLabSpecimentCHE = rs.rows;
      console.log('itemLabSpecimentCHE :',this.itemLabSpecimentCHE);


      
    } catch (error) {
      console.log('itemLabSpecimentCHE :',error);
      
    }
  }


  async ListlookupLabCHE(){
    this.itemLabCHE = [];
    let info:any={
      limit:"1000",
      LabGroup:"2"

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLabCHE = rs.rows;
      console.log('itemLabCHE :',this.itemLabCHE);


      
    } catch (error) {
      console.log('itemLabCHE :',error);
      
    }
  }


  async ListlookupLabIMM(){
    this.itemLabIMM = [];
    let info:any={
      limit:"1000",
      LabGroup:"5"

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLabIMM = rs.rows;
      console.log('itemLabIMM :',this.itemLabIMM);


      
    } catch (error) {
      console.log('itemLabIMM :',error);
      
    }
  }

  async ListlookupLabMIC(){
    this.itemLabMIC = [];
    let info:any={
      limit:"1000",
      LabGroup:"6"

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLabMIC = rs.rows;
      console.log('itemLabMIC :',this.itemLabMIC);


      
    } catch (error) {
      console.log('itemLabMIC :',error);
      
    }
  }

  async ListlookupLabBIO(){
    this.itemLabBIO = [];
    let info:any={
      limit:"1000",
      LabGroup:"1"

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLabBIO = rs.rows;
      console.log('itemLabBIO :',this.itemLabBIO);


      
    } catch (error) {
      console.log('itemLabBIO :',error);
      
    }
  }

  async ListlookupLabMIS(){
    this.itemLabMIS = [];
    let info:any={
      limit:"1000",
      LabGroup:"7"

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLabMIS = rs.rows;
      console.log('itemLabMIS :',this.itemLabMIS);


      
    } catch (error) {
      console.log('itemLabMIS :',error);
      
    }
  }


  async ListlookupLabHEM(){
    this.itemLabHEM = [];
    let info:any={
      limit:"1000",
      LabGroup:"4"

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLabHEM = rs.rows;
      console.log('itemLabHEM :',this.itemLabHEM);


      
    } catch (error) {
      console.log('itemLabHEM :',error);
      
    }
  }

  async ListlookupLabOUT(){
    this.itemLabOUT = [];
    let info:any={
      limit:"1000",
      LabGroup:"4"

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLabOUT = rs.rows;
      console.log('itemLabOUT :',this.itemLabOUT);


      
    } catch (error) {
      console.log('itemLabOUT :',error);
      
    }
  }


  async ListlookupLab(){
    this.itemLab = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      //console.log(rs);
      this.itemLab = rs.rows;
      console.log('itemLab :',this.itemLab);


      
    } catch (error) {
      console.log('itemLab :',error);
      
    }
  }
   
     
  
}