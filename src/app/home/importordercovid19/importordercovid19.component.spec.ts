import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Importordercovid19Component } from './importordercovid19.component';

describe('Importordercovid19Component', () => {
  let component: Importordercovid19Component;
  let fixture: ComponentFixture<Importordercovid19Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Importordercovid19Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Importordercovid19Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
