import { Component, OnInit } from '@angular/core';
import { AlertService} from '../../shared/alert.service';
import {LookupCommentService} from '../../shared/lookup-comment.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  txtcomment_id:any;
  txtcomment_name:any;
  txtcomment_delete:any;
  txtEnabled:any;
  txtment_id:any;
  ment_id:any;
  itemComment:any=[];
  openModal:boolean=false;
  constructor(
    private alertService:AlertService,
    private lookupCommentService:LookupCommentService
  ) { }

  ngOnInit(): void {
    this.ListComment();
  }

async ListComment(){
    this.itemComment = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupCommentService.list(info);
      console.log(rs);
      this.itemComment = rs.rows;
      console.log('itemComment :',this.itemComment);


      
    } catch (error) {
      console.log('itemComment :',error);
      
    }
  }

  async savecomment(){
    let confirm:any=await this.alertService.confirm('ท่านต้องการบันทึกหรือไม่');
    if(confirm){
    let commentdata:any={
            "comment_id": this.txtcomment_id,
            "comment_name": this.txtcomment_name,
            "Enabled": this.txtEnabled,
            
    }
    try {
      console.log(commentdata);
      
      let rs:any=await this.lookupCommentService.save(commentdata);
      console.log(rs);
    } catch (error) {
      console.log(error);
      
    }}
  }


  async formUpdate(comment:any){
    this.openModal=true;
    this.ment_id=comment.comment_id;
    this.txtcomment_id = comment.comment_id;
    this.txtcomment_name=comment.comment_name;
    this.txtEnabled=comment.Enabled;
    if(comment.Enabled=="YES"){
      this.txtEnabled=true;
    }else{
      this.txtEnabled=false;
    }
    this.ment_id=comment.comment_id;
  }



  async updatecomment(){
    
    let confirm:any=await this.alertService.confirm('ท่านต้องการแก้ไขหรือไม่');
    if(confirm){
      let enabled:any;
      if(this.txtEnabled){
        enabled="YES";
      }else{
        enabled="NO";
      }
    let commentdata:any={
            "comment_id": this.txtcomment_id,
            "comment_name": this.txtcomment_name,
            "Enabled": enabled,

            
    }
    try {
      console.log(commentdata);
      
      let rs:any=await this.lookupCommentService.update(this.ment_id,commentdata);
      console.log(rs);
      this.openModal=false;
this.ListComment();
    } catch (error) {
      console.log(error);
      
    }
  }
  }




  async frmComment(){
    this.openModal=true;
    console.log('frmComment',this.openModal);
    
  }

}
