import { Component, OnInit } from '@angular/core';
import {PatientService} from '../../shared/patient.service';
import { AlertService} from '../../shared/alert.service';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  itemPatient:any=[];
  hospital:any;
  search_text:any;
  openModal:boolean=false;

  txthn:any;
  txtprename:any;
  txtname:any;
  txtlname:any;
  txthospcode:any;
  txtcid:any;
  txtsex:any;
  txttel:any;
  txtbirthday:any;
  txtage:any;
  patient_id:any;


  constructor(
    private patientService:PatientService,
    private alertService:AlertService
    ) { }

  ngOnInit(): void {
    this.ListPatient();
    console.log('frmPatient',this.openModal);

  }

  async ListPatient(){
    this.itemPatient = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.patientService.list(info);
      console.log(rs);
      this.itemPatient = rs.rows;
      console.log('itemLab :',this.itemPatient);


      
    } catch (error) {
      console.log('itemLab :',error);
      
    }
  }


  async Search(){
    this.itemPatient = [];
    let info:any={
      limit:"1000",
      search_text:this.search_text,
      HOSCode:this.hospital

    };

    try {
      let rs:any = await this.patientService.list(info);
      console.log(rs);
      this.itemPatient = rs.rows;
      console.log('itemLab :',this.itemPatient);


      
    } catch (error) {
      console.log('itemLab :',error);
      
    }
  }


  async savepatient(){
    let confirm:any=await this.alertService.confirm('ท่านต้องการบันทึกหรือไม่');
    if(confirm){
    let patientdata:any={
      "cid": this.txtcid,
            "HIS_HN": this.txthn,
            "passport": "",
            "birth": this.txtbirthday,
            "pre_name": this.txtprename, 
            "name": this.txtname,
            "lname": this.txtlname,
            "name_eng": "",
            "lname_eng": "",
            "telephone": this.txttel,
            "address": "",
            "address_eng": "",
            "nation": "",
            "province": "", 
            "district": "",
            "sub_district": "",
            "sex": this.txtsex,
            "HOSCode": this.txthospcode
    }
    try {
      console.log(patientdata);
      
      let rs:any=await this.patientService.save(patientdata);
      console.log(rs);
      this.openModal=false;

    } catch (error) {
      console.log(error);
      
    }}
  }

  async formUpdate(patient:any){
    this.openModal=true;
    this.txtcid = patient.cid,
    this.txthn=patient.HIS_HN,
    this.txtbirthday=moment.tz(patient.birth, "Asia/Bangkok").format('YYYY-MM-DD'),
    this.txtprename=patient.pre_name,
    this.txtname=patient.name,
    this.txtlname=patient.lname,
    this.txttel=patient.telephone,
    this.txtsex=patient.sex,
    this.txthospcode=patient.HOSCode
    this.patient_id=patient.patient_id
  }

  async updatepatient(){
    
    let confirm:any=await this.alertService.confirm('ท่านต้องการแก้ไขหรือไม่');
    if(confirm){
    let patientdata:any={
      "cid": this.txtcid,
            "HIS_HN": this.txthn,
            "passport": "",
            "birth": this.txtbirthday,
            "pre_name": this.txtprename, 
            "name": this.txtname,
            "lname": this.txtlname,
            "name_eng": "",
            "lname_eng": "",
            "telephone": this.txttel,
            "address": "",
            "address_eng": "",
            "nation": "",
            "province": "", 
            "district": "",
            "sub_district": "",
            "sex": this.txtsex,
            "HOSCode": this.txthospcode
    }
    try {
      console.log(patientdata);
      
      let rs:any=await this.patientService.update(this.patient_id,patientdata);
      console.log(rs);
      this.openModal=false;
this.ListPatient();
    } catch (error) {
      console.log(error);
      
    }
  }
  }


async frmPatient(){
    this.openModal=true;
    console.log('frmPatient',this.openModal);
    
  }



}
