import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { PatientComponent } from './patient/patient.component';
import { CheckresultComponent } from './checkresult/checkresult.component';
import { ResultmanualComponent } from './resultmanual/resultmanual.component';
import { Importordercovid19Component } from './importordercovid19/importordercovid19.component';
import { ExportorderComponent } from './exportorder/exportorder.component';
import { PrintsummaryorderComponent } from './printsummaryorder/printsummaryorder.component';
import { SummaryallhospitalComponent } from './summaryallhospital/summaryallhospital.component';
import { SummarybyhospitalComponent } from './summarybyhospital/summarybyhospital.component';
import { SummaryalltestComponent } from './summaryalltest/summaryalltest.component';
import { SummarybytestComponent } from './summarybytest/summarybytest.component';
import { OrderentryComponent } from './orderentry/orderentry.component';
import { HospitalComponent } from './hospital/hospital.component';
import { CommentComponent } from './comment/comment.component';
import { GroupsComponent } from './groups/groups.component';
import { SpecimensComponent } from './specimens/specimens.component';
import { LabsComponent } from './labs/labs.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { AppointComponent } from './appoint/appoint.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { SampleresultComponent } from './sampleresult/sampleresult.component';
import { HomeComponent } from './home.component';

const routes: Routes = [{
  path: 'home', component: LayoutComponent,
  children: [
    { path: '', redirectTo: 'patient', pathMatch: 'full' }, 
    { path: 'patient', component: PatientComponent, },
    { path: 'checkresult', component: CheckresultComponent, },
    { path: 'resultmanual', component: ResultmanualComponent, },
    { path: 'importordercovid19', component: Importordercovid19Component, },
    { path: 'exportorder', component: ExportorderComponent, },
    { path: 'printsummaryorder', component: PrintsummaryorderComponent, },
    { path: 'summaryallhospital', component: SummaryallhospitalComponent, },
    { path: 'summarybyhospital', component: SummarybyhospitalComponent, },
    { path: 'summaryalltest', component: SummaryalltestComponent, },
    { path: 'summarybytest', component: SummarybytestComponent, },
    { path: 'orderentry', component: OrderentryComponent, },
    { path: 'hospital', component: HospitalComponent, },
    { path: 'comment', component: CommentComponent, },
    { path: 'groups', component: GroupsComponent, },
    { path: 'specimens', component: SpecimensComponent, },
    { path: 'labs', component: LabsComponent, },
    { path: 'profiles', component: ProfilesComponent, },
    { path: 'useraccount', component: UseraccountComponent, },
    { path: 'appoint', component: AppointComponent, },
    { path: 'holidays', component: HolidaysComponent, },
    { path: 'sampleresult', component: SampleresultComponent, },
    
    
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
