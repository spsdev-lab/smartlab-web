import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummarybytestComponent } from './summarybytest.component';

describe('SummarybytestComponent', () => {
  let component: SummarybytestComponent;
  let fixture: ComponentFixture<SummarybytestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummarybytestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummarybytestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
