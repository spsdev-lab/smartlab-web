import { Component, OnInit } from '@angular/core';
import { AlertService} from '../../shared/alert.service';
import {LookupLabGroupService} from '../../shared/lookup-lab-group.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  txtCode:any;
  txtGroup_Name:any;
  txtSeq:any;
  txtEnabled:any;
  id_group_lab:any;
  code:any;
  itemGroup:any=[];
  openModal:boolean=false;
  constructor(
    private alertService:AlertService,
    private lookupLabGroupService:LookupLabGroupService
  ) { }

  ngOnInit(): void {
    this.ListComment();
  }

async ListComment(){
    this.itemGroup = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupLabGroupService.list(info);
      console.log(rs);
      this.itemGroup = rs.rows;
      console.log('itemGroup :',this.itemGroup);


      
    } catch (error) {
      console.log('itemGroup :',error);
      
    }
  }

  async savecomment(){
    let confirm:any=await this.alertService.confirm('ท่านต้องการบันทึกหรือไม่');
    if(confirm){
    let groupsdata:any={
            "Code": this.txtCode,
            "Group_Name": this.txtGroup_Name,
            "Seq": this.txtSeq,
            "Enabled": this.txtEnabled,
            
    }
    try {
      console.log(groupsdata);
      
      let rs:any=await this.lookupLabGroupService.save(groupsdata);
      console.log(rs);
    } catch (error) {
      console.log(error);
      
    }}
  }


  async formUpdate(groups:any){
    this.openModal=true;
    this.code=groups.Code;
    this.txtCode = groups.Code;
    this.txtGroup_Name=groups.Group_Name;
    this.txtSeq = groups.Seq;
    this.txtEnabled=groups.Enabled;
    if(groups.Enabled=="YES"){
      this.txtEnabled=true;
    }else{
      this.txtEnabled=false;
    }
    this.code=groups.Code;
  }



  async updategroups(){
    
    let confirm:any=await this.alertService.confirm('ท่านต้องการแก้ไขหรือไม่');
    if(confirm){
      let enabled:any;
      if(this.txtEnabled){
        enabled="YES";
      }else{
        enabled="NO";
      }
    let groupsdata:any={
            "Code": this.txtCode,
            "Group_Name": this.txtGroup_Name,
            "Seq": this.txtSeq,
            "Enabled": enabled,

            
    }
    try {
      console.log(groupsdata);
      
      let rs:any=await this.lookupLabGroupService.update(this.code,groupsdata);
      console.log(rs);
      this.openModal=false;
this.ListComment();
    } catch (error) {
      console.log(error);
      
    }
  }
  }




  async frmGroups(){
    this.openModal=true;
    console.log('frmGroups',this.openModal);
    
  }

}
