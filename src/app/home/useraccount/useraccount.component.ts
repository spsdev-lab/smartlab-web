import { Component, OnInit } from '@angular/core';
import { AlertService} from '../../shared/alert.service';
import { UsersService } from '../../shared/users.service';

@Component({
  selector: 'app-useraccount',
  templateUrl: './useraccount.component.html',
  styleUrls: ['./useraccount.component.scss']
})
export class UseraccountComponent implements OnInit {
  txthospcode:any;
  txtusercode:any;
  txtpassword:any;
  txtfirstname:any;
  txtlastname:any;
  txtcid:any;
  userid:any;
  itemUseraccount:any=[];
  openModal:boolean=false;
  constructor(
    private alertService:AlertService,
    private usersService:UsersService
  ) { }

  ngOnInit(): void {
    this.ListHospital();
  }

  async ListHospital(){
    this.itemUseraccount = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.usersService.list(info);
      console.log(rs);
      this.itemUseraccount = rs.rows;
      console.log('itemUseraccount :',this.itemUseraccount);


      
    } catch (error) {
      console.log('itemUseraccount :',error);
      
    }
  }

  async saveuseraccount(){
    let confirm:any=await this.alertService.confirm('ท่านต้องการบันทึกหรือไม่');
    if(confirm){
    let useraccountdata:any={
            "HosCode": this.txthospcode,
            "Password": this.txtpassword,
            "UserCode": this.txtusercode,
            "FirstName": this.txtfirstname,
            "LastName": this.txtlastname,
            "Cid": this.txtcid,
            
    }
    try {
      console.log(useraccountdata);
      
      let rs:any=await this.usersService.save(useraccountdata);

      this.ListHospital();
      console.log(rs);
    } catch (error) {
      console.log(error);
      
    }}
  }

  async formUpdate(useraccount:any){
    this.openModal=true;
    this.txthospcode = useraccount.HosCode;
    this.txtpassword = useraccount.Password;
    this.txtusercode=useraccount.UserCode;
    this.txtfirstname=useraccount.FirstName;
    this.txtlastname=useraccount.LastName;
    this.txtcid=useraccount.Cid;
    this.userid=useraccount.UserCode;
  }


  async updateuseraccount(){
    
    if(!this.userid){
      this.saveuseraccount();
    }else{

    let confirm:any=await this.alertService.confirm('ท่านต้องการแก้ไขหรือไม่');
    if(confirm){
      let enabled:any;

    let useraccountdata:any={
            "UserCode": this.txtusercode,
            "Password": this.txtpassword,
            "FirstName": this.txtfirstname,
            "LastName": this.txtlastname,
            "Cid": this.txtcid,
            "HosCode": this.txthospcode,
            
    }
    try {
      console.log(useraccountdata);
      
      let rs:any=await this.usersService.update(this.txtusercode,useraccountdata);
      this.ListHospital();

      this.userid=null;
      this.txtusercode=null;
      this.txtpassword=null;
      this.txtfirstname=null;
      this.txtlastname=null;
      this.txtcid=null;
      this.txthospcode=null;
      console.log(rs);
      this.openModal=false;
this.ListHospital();
    } catch (error) {
      console.log(error);
      
    }
  }}
  }


  async frmUseraccount(){
    this.userid=null;
    this.openModal=true;
    
    
  }

}
