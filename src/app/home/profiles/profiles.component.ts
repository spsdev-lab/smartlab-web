import { Component, OnInit } from '@angular/core';
import { AlertService} from '../../shared/alert.service';
import { LookupLabService } from '../../shared/lookup-lab.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {

  txtlccode:any;
  txtcode:any;
  txtlabname:any;
  txtlabgroup:any;
  txtspecimen:any;
  txtprice_gov:any;
  txtprice:any;
  txtlaborder:any;
  txtkey_manual:any;
  txtprofile:any;
  txthos_id:any;
  lccode:any;
  itemLab:any=[];
  txtenabled:any;
  openModal:boolean=false;
  constructor(
    private alertService:AlertService,
    private lookupLabService:LookupLabService
  ) { }

  ngOnInit(): void {
    this.ListLab();
  }

  async ListLab(){
    this.itemLab = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupLabService.list(info);
      console.log(rs);
      this.itemLab = rs.rows;
      console.log('itemHospital :',this.itemLab);


      
    } catch (error) {
      console.log('itemHospital :',error);
      
    }
  }

  async savelab(){
    let confirm:any=await this.alertService.confirm('ท่านต้องการบันทึกหรือไม่');
    if(confirm){
    let labdata:any={
            "LC_Code": this.txtlccode,
            "Code": this.txtcode,
            "Lab_Name": this.txtlabname,
            "LabGroup": this.txtlabgroup,
            "Specimen": this.txtspecimen,
            "Price_gov": this.txtprice_gov,
            "Price": this.txtprice,
            "LabOrder": this.txtlaborder,
            "Key_Manual": this.txtkey_manual,
            "Profile": this.txtprofile,
            "Enabled": this.txtenabled,
            
    }
    try {
      console.log(labdata);
      
      let rs:any=await this.lookupLabService.save(labdata);
      console.log(rs);
    } catch (error) {
      console.log(error);
      
    }}
  }

  async formUpdate(lab:any){
    this.openModal=true;
    this.txtlccode=lab.LC_Code;
    this.txtcode = lab.Code;
    this.txtlabname=lab.Lab_Name;
    this.txtlabname=lab.LabGroup;
    this.txtlabname=lab.Price_gov;
    this.txtprice=lab.Price;
    this.txtlaborder=lab.LabOrder;
    this.txtkey_manual=lab.Key_Manual;
    this.txtprofile=lab.Profile;
    this.txtenabled=lab.Enabled;
    if(lab.Enabled=="YES"){
      this.txtenabled=true;
    }else{
      this.txtenabled=false;
    }
    this.lccode=lab.hos_id;
  }


  async updatelab(){
    
    let confirm:any=await this.alertService.confirm('ท่านต้องการแก้ไขหรือไม่');
    if(confirm){
      let enabled:any;
      if(this.txtenabled){
        enabled="YES";
      }else{
        enabled="NO";
      }
    let labdata:any={
      "LC_Code": this.txtlccode,
      "Code": this.txtcode,
      "Lab_Name": this.txtlabname,
      "LabGroup": this.txtlabgroup,
      "Specimen": this.txtspecimen,
      "Price_gov": this.txtprice_gov,
      "Price": this.txtprice,
      "LabOrder": this.txtlaborder,
      "Key_Manual": this.txtkey_manual,
      "Profile": this.txtprofile,
      "Enabled": this.txtenabled,
            
    }
    try {
      console.log(labdata);
      
      let rs:any=await this.lookupLabService.update(this.lccode,labdata);
      console.log(rs);
      this.openModal=false;
this.ListLab();
    } catch (error) {
      console.log(error);
      
    }
  }
  }


  async frmLab(){
    this.openModal=true;
    console.log('frmLab',this.openModal);
    
  }

}
