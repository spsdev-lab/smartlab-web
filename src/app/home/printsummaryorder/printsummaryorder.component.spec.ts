import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintsummaryorderComponent } from './printsummaryorder.component';

describe('PrintsummaryorderComponent', () => {
  let component: PrintsummaryorderComponent;
  let fixture: ComponentFixture<PrintsummaryorderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintsummaryorderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintsummaryorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
