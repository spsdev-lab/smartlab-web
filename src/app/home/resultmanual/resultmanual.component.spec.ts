import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultmanualComponent } from './resultmanual.component';

describe('ResultmanualComponent', () => {
  let component: ResultmanualComponent;
  let fixture: ComponentFixture<ResultmanualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultmanualComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultmanualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
