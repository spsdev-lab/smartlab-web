import { Component, OnInit } from '@angular/core';
import {LookupHoscodeService} from '../../shared/lookup-hoscode.service';
import{LookupLabGroupService} from '../../shared/lookup-lab-group.service';
import {LabresultService} from '../../shared/labresult.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkresult',
  templateUrl: './checkresult.component.html',
  styleUrls: ['./checkresult.component.scss']
})
export class CheckresultComponent implements OnInit {
  itemHoscode:any=[];
  itemLabGroup:any=[];
  itemResult:any=[];
  hos_id:any;
  id_group_lab:any;
  req_no:any;
  txtsearch:any;
  constructor(
    private lookupHoscodeService:LookupHoscodeService,
    private lookupLabGroupService:LookupLabGroupService,
    private labresultService:LabresultService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.ListlookupHoscode();
    this.ListlookupLabGroup();
    this.labresult();

  }

  async opensample(result:any){
    let resultinfo:any=JSON.stringify(result);
    sessionStorage.setItem('resultinfo', resultinfo);
    this.router.navigate(["/home/sampleresult"]);

  }

  

  async ListlookupHoscode(){
    this.itemHoscode = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupHoscodeService.list(info);
      //console.log(rs);
 
      this.itemHoscode = rs.rows;
      // this.itemHoscode.push({hos_id:' ',hos_name:'กรุณาเลือก',Enabled:''});
      console.log('itemHoscode :',this.itemHoscode);


      
    } catch (error) {
      console.log('itemHoscode :',error);
      
    }
  }

  async ListlookupLabGroup(){
    this.itemLabGroup = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupLabGroupService.list(info);
      //console.log(rs);
 
      this.itemLabGroup = rs.rows;
      // this.itemLabGroup.push({hos_id:' ',hos_name:'กรุณาเลือก',Enabled:''});
      console.log('itemLabGroup :',this.itemLabGroup);


      
    } catch (error) {
      console.log('itemLabGroup :',error);
      
    }
  }

  async labresult(){
    this.itemResult = [];
    let info:any={
      "req_no":this.req_no,
      "txtSearch":this.txtsearch,
      "HosCode":this.hos_id
    };
    console.log(info);
    

    try {
      let rs:any = await this.labresultService.list(info);
      //console.log(rs);
 
      this.itemResult = rs;
      // this.itemResult.push({hos_id:' ',hos_name:'กรุณาเลือก',Enabled:''});
      console.log('itemResult :',this.itemResult);


      
    } catch (error) {
      console.log('itemResult :',error);
      
    }
  }

}
