import { Component, OnInit } from '@angular/core';
import {LabresultService} from '../../shared/labresult.service';
import {LabreqDService} from '../../shared/labreq-d.service';
import {PatientService} from '../../shared/patient.service';
import * as moment from 'moment-timezone';


@Component({
  selector: 'app-sampleresult',
  templateUrl: './sampleresult.component.html',
  styleUrls: ['./sampleresult.component.scss']
})
export class SampleresultComponent implements OnInit {
  itemPatient:any=[];
  itemReq:any=[];
  fullname:any;
  patient_id:any;
  cid:any;
  resultinfo:any;
  UserCode:any;
  Fullname:any;
  HosCode:any;
  constructor(
    private labreqDService:LabreqDService,
    private patientService:PatientService
  ) { 
let info:any=sessionStorage.getItem('resultinfo');

    this.resultinfo = JSON.parse(info);
    console.log(this.resultinfo);
    this.ListPatient(this.resultinfo.HN);

    this.Fullname = sessionStorage.getItem('Fullname');
    this.UserCode = sessionStorage.getItem('UserCode');
    this.HosCode = sessionStorage.getItem('HosCode');
    
  }

  ngOnInit(): void {

  }


  async savelab(req:any){
    let id_reqd= req.id_reqd;
    let inforeq_d:any={
      "req_no":req.req_no,
      "authDateTime": moment.tz(new Date(), "Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss'),
      "authUser": this.UserCode,
  }
  try {
    let rs:any=await this.labreqDService.update(id_reqd,inforeq_d);
    window.location.reload();
    console.log(rs);
    
  } catch (error) {
    console.log(error);
    
  }

  }


  async ListPatient(hn:any){
    this.itemPatient = [];
    let info:any={
      "limit":"1000",
      "HIS_HN":hn
    };

    try {
      let rs:any = await this.patientService.list(info);
      console.log(rs);
      this.itemPatient = rs.rows;
      console.log('itemPatient :',this.itemPatient[0]);
      this.patient_id=this.itemPatient[0].patient_id;
      this.ListlabreqD(this.patient_id);


      
    } catch (error) {
      console.log('itemPatient :',error);

      
    }
  }

  async ListlabreqD(patient_id:any){
    this.itemReq = [];
    let info:any={
      "limit":"1000",
      "patient_id":patient_id

    };

    try {
      let rs:any = await this.labreqDService.list(info);
      //console.log(rs);
      this.itemReq = rs.rows;
      console.log('itemReq :',this.itemReq);


      
    } catch (error) {
      console.log('itemReq :',error);
      
    }
  }

}
