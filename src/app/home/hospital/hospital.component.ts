import { Component, OnInit } from '@angular/core';
import { AlertService} from '../../shared/alert.service';
import {LookupHoscodeService} from '../../shared/lookup-hoscode.service';
@Component({
  selector: 'app-hospital',
  templateUrl: './hospital.component.html',
  styleUrls: ['./hospital.component.scss']
})
export class HospitalComponent implements OnInit {
  txthospcode:any;
  txthospname:any;
  txthosptype:any;
  txtenabled:any;
  txthos_id:any;
  hos_id:any;
  itemHospital:any=[];
  openModal:boolean=false;
  constructor(
    private alertService:AlertService,
    private lookupHoscodeService:LookupHoscodeService
  ) { }

  ngOnInit(): void {
    this.ListHospital();
  }

  async ListHospital(){
    this.itemHospital = [];
    let info:any={
      limit:"1000",

    };

    try {
      let rs:any = await this.lookupHoscodeService.list(info);
      console.log(rs);
      this.itemHospital = rs.rows;
      console.log('itemHospital :',this.itemHospital);


      
    } catch (error) {
      console.log('itemHospital :',error);
      
    }
  }

  async savehospital(){
    let confirm:any=await this.alertService.confirm('ท่านต้องการบันทึกหรือไม่');
    if(confirm){
    let hospitaldata:any={
            "hospcode": this.txthospcode,
            "hospname": this.txthospname,
            "Enabled": this.txtenabled,
            
    }
    try {
      console.log(hospitaldata);
      
      let rs:any=await this.lookupHoscodeService.save(hospitaldata);
      console.log(rs);
    } catch (error) {
      console.log(error);
      
    }}
  }

  async formUpdate(hospital:any){
    this.openModal=true;
    this.hos_id=hospital.hos_id;
    this.txthospcode = hospital.hcode;
    this.txthospname=hospital.hos_name;
    this.txtenabled=hospital.Enabled;
    if(hospital.Enabled=="YES"){
      this.txtenabled=true;
    }else{
      this.txtenabled=false;
    }
    this.hos_id=hospital.hos_id;
  }


  async updatehospital(){
    
    let confirm:any=await this.alertService.confirm('ท่านต้องการแก้ไขหรือไม่');
    if(confirm){
      let enabled:any;
      if(this.txtenabled){
        enabled="YES";
      }else{
        enabled="NO";
      }
    let hospitaldata:any={
            "hos_id": this.txthos_id,
            "hos_name": this.txthospname,
            "Enabled": enabled,
            "hcode": this.txthospcode,
            
    }
    try {
      console.log(hospitaldata);
      
      let rs:any=await this.lookupHoscodeService.update(this.hos_id,hospitaldata);
      console.log(rs);
      this.openModal=false;
this.ListHospital();
    } catch (error) {
      console.log(error);
      
    }
  }
  }


  async frmHospital(){
    this.openModal=true;
    console.log('frmHospital',this.openModal);
    
  }

}
