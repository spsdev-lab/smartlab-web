import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryallhospitalComponent } from './summaryallhospital.component';

describe('SummaryallhospitalComponent', () => {
  let component: SummaryallhospitalComponent;
  let fixture: ComponentFixture<SummaryallhospitalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryallhospitalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryallhospitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
