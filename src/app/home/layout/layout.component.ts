import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  Fullname:any;
  UserCode:any;
  HosCode:any;
  hcode:any;
  constructor(
    private router:Router,
  ) { 
    this.Fullname = sessionStorage.getItem('Fullname');
    console.log(this.Fullname);
    
    this.UserCode = sessionStorage.getItem('UserCode');
    this.HosCode = sessionStorage.getItem('HosCode');
    this.hcode=this.HosCode;
  }

  ngOnInit(): void {
  }


  logout(){
    sessionStorage.removeItem('Fullname');
    sessionStorage.removeItem('UserCode');
    sessionStorage.removeItem('HosCode');
    this.router.navigate(["/login"]);

  }

}
